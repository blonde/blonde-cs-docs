# FAQ
## How do I upload content
* Use the upload` action button
* Hit Add file` to open the file browser window
* Select the file(s) you want to upload on your computer
* Hit `OK`button

The selected files will be uploaded in background and added to the upload panel on screen.