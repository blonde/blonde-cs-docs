# Bulk uploading content
The Blondé Content Store support bulk uploading of content to e.g. initialize your tenant environment or 
add additional content later on.

**Remark**
Make sure you know your tenant's ID (`<id>`) within Blondé's Content Store as it defines your buckets on AWS where 
your content is stored and where you can upload your content.
 
## How it works
To bulk upload your content, you'll simply have to upload your content files to your designated tenant's 
upload bucket on AWS S3. You can do this via 

* the **AWS command-line** which is pretty neat for automating content loads; or 
* via a **3rd party GUI tool** like Cyberduck, which is easy to use and uploading is as simple as drag and drop.
 
## Preparing your content
To upload a single file `<filename>.<ext>`, you have to transform each file into 2 files with the same unique name.

**Procedure**

* Generate a UUID v4 for your file, e.g. go to [https://www.uuidgenerator.net/](https://www.uuidgenerator.net/) and 
simply copy and paste the generated ID there. Let's call this the `<uuid>`.
* Create a meta-information object file for you content; name it '<uuid>.mson'.
	* Edit the mson file so it contains the information as underneath. You can define the `<meta-key>: <meta-value>` combinations yourself to whathever you consider relevant.

```javascript
{
    "id": "<uuid>",
    "title": "<filename>",
    "filename": "<uuid>",
    "basename": "<id>-cs/<uuid>",
    "owner": "<userid>",
    "type": "<content-type>",
    "<meta-key>": "<meta-value>",
    ...
    "contents": [{
        "rendition": "default",
        "mimetype": "application/<ext>",
        "size": <file's size>
    }]
}
```
* Rename your file from `<filename>.<ext>` to `<uuid>.<ext>`

## Uploading your content
Once you have prepared all your content and assembled all your files to be uploaded in one folder on you computer, upload all these files simply to your designated AWS upload bucket `blonde-cs-upload/<id>-cs`. 

The upload itself will trigger the processing on our backbone servers to 
* load your content into the content buckets;
* to store your meta information for each object in our store; and
* to generate a thumbnail for you files.

### That's all folks!