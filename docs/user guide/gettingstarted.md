# Getting Started 

Let's get started with Blondé's Content Store and start liquifying your company's content!

## Navigating around
* The logo on each pages, take's you back home. Just click it and try!
* The search box allows you to textual find all your content and hints you in specifying search terms on specific fields.
* You profile brings you to your personal settings or, in case you are your tenant's administrator, gives you access to your tenant's environment settings.
* The *hamburger* menu gives you access to all the functions Blondé's Content Store has in petto for you!

## Table of Contents

*TBD*

